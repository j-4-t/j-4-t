package com.javafortesters.arrays;

/**
 * Created by rb on 24-11-14.
 */
public class ArrayPrinter {

    public void print2DIntArray(int[][] multi) {
        for (int[] outer : multi) {
            if (outer == null) {
                System.out.print("null");
            } else {
                for (int inner : outer) {
                    System.out.print(inner + ",");
                }
            }
            System.out.println("");
        }
    }

    public void print2DIntArrayClassic(int[][] multi) {
        for (int i = 0; i < multi.length ; ++i) {

            int[] outer = multi[i];

            if (outer == null) {
                System.out.print("null");
            } else {
                for (int j = 0; j < outer.length; ++j) {
                    int inner = outer[j];
                    System.out.print(inner + ",");
                }
            }
            System.out.println("");
        }
    }

    public void print2DIntArrayClassicBonus(int[][] multi) {
        int i = 0;
        for (; ;) {

            int[] outer = multi[i];

            if (outer == null) {
                System.out.print("null");
            } else {
                int j = 0;
                for (; ; ) {
                    int inner = outer[j];
                    System.out.print(inner + ",");

                    ++j;
                    if (j >= outer.length) break;
                }
            }
            System.out.println("");

            ++i;
            if (i >= multi.length) break;
        }
    }
}
