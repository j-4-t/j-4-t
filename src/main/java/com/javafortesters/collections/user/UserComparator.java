package com.javafortesters.collections.user;


import java.util.Comparator;

public class UserComparator implements Comparator<User> {

    @Override
    public int compare(User user1, User user2) {
        if(user1.getUsername().compareTo(user2.getUsername())==0){
            return 0;
        }
        int user1Comparator = user1.getPassword().length() +
                user1.getUsername().length();
        int user2Comparator = user2.getPassword().length() +
                user2.getUsername().length();
        int val = user1Comparator - user2Comparator;
        if(val==0){
            val = user1.getUsername().compareTo(user2.getUsername());
        }
        return val;
    }
}
