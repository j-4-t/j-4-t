package com.javafortesters.domainentities;

/**
 * Created by rb on 17-10-14.
 */
public class User {

    public static final String DEFAULT_USERNAME = "username";

    public static final String DEFAULT_PASSWORD = "password";
    private static final String PASSWORD_INCLUDES_UPPERCASE_REGEX = ".*[A-Z]+.*";
    private static final String PASSWORD_INCLUDES_DIGIT_REGEX = ".*[0-9]+.*";

    private String username;
    private String password;

    public User() {
        this(DEFAULT_USERNAME, DEFAULT_PASSWORD);
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        if (!password.matches(PASSWORD_INCLUDES_UPPERCASE_REGEX) || ! password.matches(PASSWORD_INCLUDES_DIGIT_REGEX)) {
            throw new IllegalPasswordException();
        }
        this.password = password;
    }
}
