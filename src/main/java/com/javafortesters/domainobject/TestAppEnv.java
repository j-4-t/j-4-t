package com.javafortesters.domainobject;

/**
 * Created by rb on 17-10-14.
 */
public class TestAppEnv {

    public static final java.lang.String DOMAIN = "192.123.0.3";
    public static final java.lang.String PORT = "67";
    public static final java.lang.String PROTOCOL = "http://";

    public static String getUrl() {
        return PROTOCOL + DOMAIN + ":" + PORT;
    }
}
