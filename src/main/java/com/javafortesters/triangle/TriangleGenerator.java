package com.javafortesters.triangle;

public class TriangleGenerator {

    public int[][] generateTriangle(int triangleSize) {

        int [][] triangle = new int[triangleSize+1][];

        for (int i = 0; i <= triangleSize; ++i) {
            triangle[i] = new int[i+1];
            for (int j = 0; j <= i; ++j) {
                triangle[i][j] = j;
            }
        }
        return triangle;
    }

    public int[][] generateSymmetricTriangle(int triangleSize) {

        int [][] triangle = new int[triangleSize*2+1][];

        int pos = 0;

        for (int i = 0; i <= triangleSize; ++i) {
            triangle[pos] = new int[i+1];
            for (int j = 0; j <= i; ++j) {
                triangle[pos][j] = j;
            }
            pos++;
        }

        for (int i = triangleSize-1; i >=0; i--) {
            triangle[pos] = new int[i+1];
            for (int j = i; j >= 0; j--) {
                triangle[pos][j] = j;
            }
            pos++;
        }
        return  triangle;
    }
}
