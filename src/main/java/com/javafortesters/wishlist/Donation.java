package com.javafortesters.wishlist;

public class Donation extends Gift {

    private String beneficiary;


    public Donation() {
        super();
    }

    public Donation(String name, String beneficiary) {
        super(name);
        this.beneficiary = beneficiary;
    }

    public Donation(String beneficiary) {
        super();
        this.beneficiary = beneficiary;
    }

    public void setBeneficiary(String beneficiary) {
        this.beneficiary = beneficiary;
    }

    public String getBeneficiary() {
        return beneficiary;
    }

    @Override
    public String toString() {
        return "Donation{" +
                "name='" + getName() + "\', " +
                "beneficiary='" + beneficiary + "\'" +
                '}';
    }
}
