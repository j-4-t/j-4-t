package com.javafortesters.wishlist;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by rb on 25-11-14.
 */
public class Gift implements Comparable<Gift> {

    public static final String DEFAULT_GIFT_NAME = "Surprise";

    private String name;
    private BigDecimal price;
    private String description;
    private Date wishedDate;

    public Gift(String name) {
        this.name = name;
        this.price = BigDecimal.ZERO;
        this.wishedDate = new Date();
    }

    public Gift() {
        this(DEFAULT_GIFT_NAME);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setWishedDate(Date wishedDate) {
        this.wishedDate = wishedDate;
    }

    public Date getWishedDate() {
        return wishedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gift gift = (Gift) o;

        if (name != null ? !name.equals(gift.name) : gift.name != null) return false;
        if (price != null ? !price.equals(gift.price) : gift.price != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Gift{" +
                "name='" + name + '\'' +
                '}';
    }


    @Override
    public int compareTo(Gift o) {
        return this.getWishedDate().compareTo(o.getWishedDate());
    }
}
