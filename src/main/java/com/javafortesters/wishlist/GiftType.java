package com.javafortesters.wishlist;

/**
 * Created by rb on 26-11-14.
 */
public enum GiftType {
    Active, Educational, Other
}
