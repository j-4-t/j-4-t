package com.javafortesters.wishlist;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by rb on 25-11-14.
 */
public class WishList {

    private static final String DEFAULT_NAME = "Unnamed";

    private String name;

    private List<Gift> gifts;

    private int maximumGifts;

    final private Random generate;

    public WishList(String name) {
        this.name = name;
        this.gifts = new ArrayList<Gift>();
        generate = new Random(1234567890);
    }

    public WishList() {
        this(DEFAULT_NAME);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Gift> getGifts() {
        Collections.sort(gifts);
        return gifts;
    }

    public void addGift(Gift gift) {
        if (maximumGifts > 0 && gifts.size() == maximumGifts) {
            throw new WishListFullException("Exceeded the maximum of " + maximumGifts + " item(s) of the wishlist", gift);
        }
        gifts.add(gift);
    }

    public void removeGift(Gift gift) {
        gifts.remove(gift);
    }

    public void removeGifts() {
        gifts.clear();
    }

    public int getTotalGiftCount() {
        return gifts.size();
    }

    public BigDecimal getTotalPriceOfGifts() {
        BigDecimal sum = new BigDecimal(0);
        for (Gift gift : gifts) {
            sum = sum.add(gift.getPrice());
        }
        return sum;
    }

    public Gift getMostExpensiveGift() {
        Gift mostExpensiveGift = null;
        for (Gift gift : this.getGifts()) {
            if (mostExpensiveGift == null || gift.getPrice().compareTo(mostExpensiveGift.getPrice()) == 1) {
                mostExpensiveGift = gift;
            }
        }
        return mostExpensiveGift;
    }

    public Gift getLeastExpensiveGift() {
        Gift leastExpensiveGift = null;
        for (Gift gift : this.getGifts()) {
            if (leastExpensiveGift == null || gift.getPrice().compareTo(leastExpensiveGift.getPrice()) == -1) {
                leastExpensiveGift = gift;
            }
        }
        return leastExpensiveGift;
    }

    @Override
    public String toString() {
        return "WishList{" +
                "name='" + name + '\'' +
                ",gifts=" + gifts.size() +
                '}';
    }

    public void addGifts(WishList otherWishList) {
        gifts.addAll(otherWishList.getGifts());
    }

    public Gift findGiftWithNameLike(String like) {
        Gift result = null;
        for (Gift gift : this.getGifts()) {
            if (gift.getName().contains(like)) {
                result = gift;
                break;
            }
        }
        return result;
    }


    public void setMaximumGifts(int maximumGifts) {
        this.maximumGifts = maximumGifts;
    }

    public int getMaximumGifts() {
        return maximumGifts;
    }

    public static GiftType determineGiftType(Gift gift) {
        switch (gift.getName().toLowerCase()) {
            case "ball":
            case "bike":
                return GiftType.Active;
            case "book":
            case "pen":
            case "laptop":
            case "paper":
                return GiftType.Educational;
            default:
                return GiftType.Other;
        }
    }

    public static Map<GiftType, Integer> determineGiftType(WishList wishList) {
        Map<GiftType, Integer> result = new HashMap<GiftType, Integer>();

        for (GiftType giftType : GiftType.values()) {
            result.put(giftType, 0);
        }

        for (Gift gift : wishList.getGifts()) {
            GiftType type = determineGiftType(gift);
            result.put(type, result.get(type) + 1);
        }
        return result;
    }

    public Gift getRandomGift() {
        return gifts.get(generate.nextInt(gifts.size()));
    }



    public List<Gift> getGiftsInPriceRange(BigDecimal priceFromInclusive, BigDecimal priceToInclusive) {
        List<Gift> result = new ArrayList<>();

        for (Gift gift : gifts) {
            if (gift.getPrice().compareTo(priceFromInclusive) >=0 && gift.getPrice().compareTo(priceToInclusive) <= 0) {
                result.add(gift);
            }
        }
        return result;
    }

    public Gift getRandomGift(BigDecimal priceFromInclusive, BigDecimal priceToInclusive) {
        List<Gift> giftsInPriceRange = getGiftsInPriceRange(priceFromInclusive, priceToInclusive);
        return giftsInPriceRange.get(generate.nextInt(giftsInPriceRange.size()));
    }
}

