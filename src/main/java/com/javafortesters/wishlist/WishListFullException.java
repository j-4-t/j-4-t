package com.javafortesters.wishlist;

/**
 * Created by rb on 25-11-14.
 */
public class WishListFullException extends RuntimeException {

    private Gift gift;

    public WishListFullException(String message, Gift gift) {
        super(message);
        this.gift = gift;
    }

    public Gift getGift() {
        return gift;
    }
}
