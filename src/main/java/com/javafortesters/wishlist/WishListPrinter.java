package com.javafortesters.wishlist;

/**
 * Created by rb on 2-1-15.
 */
public class WishListPrinter {

    private final static String LINE = "--------------------------------------------------\n";


    public static void print(WishList wishList) {
        printHeader(wishList);
        printGifts(wishList);
        printTotals(wishList);
        System.out.print(LINE);
    }

    public static void printHeader(WishList wishList) {
        StringBuilder sb = new StringBuilder();

        sb.append(LINE);
        sb.append(wishList.getName().toUpperCase());
        sb.append(":\n");
        sb.append(LINE);

        System.out.print(sb.toString());
    }

    public static void printGifts(WishList wishList) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < wishList.getTotalGiftCount(); ++i) {
            Gift gift = wishList.getGifts().get(i);
            sb.append(i + 1);
            sb.append(".\t");
            sb.append(gift.getName());
            sb.append("\t:\t");
            sb.append((gift.getDescription() == null) ? "<no description>" : gift.getDescription());
            sb.append("\t:\t");
            sb.append(String.format("%.2f", gift.getPrice()));
            sb.append("\n");

        }
        System.out.print(sb.toString());

    }

    public static void printTotals(WishList wishList) {
        StringBuilder sb = new StringBuilder();

        sb.append(LINE);
        sb.append("TOTALS:\n");
        sb.append(LINE);
        sb.append("Total gifts:\t");
        sb.append(wishList.getTotalGiftCount());
        sb.append(" of max. ");
        sb.append(wishList.getMaximumGifts());
        sb.append("\n");
        sb.append("Total amount:\t");
        sb.append(String.format("%.2f", wishList.getTotalPriceOfGifts()));
        sb.append("\n");

        System.out.print(sb.toString());
    }

    public static String formatGiftName(String giftName) {
        StringBuilder result = new StringBuilder(giftName.length());
        String[] words = giftName.split(" ");

        for (String word : words) {

            if (word.length() > 0) {
                result.append(word.substring(0, 1).toUpperCase());
            }
            if (word.length() > 1) {
                result.append(word.substring(1).toLowerCase());
            }
            result.append(" ");
        }
        return result.toString().trim();
    }
}
