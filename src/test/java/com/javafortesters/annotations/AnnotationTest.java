package com.javafortesters.annotations;

import org.junit.*;

public class AnnotationTest {

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Executed before each class");
    }

    @Before
    public void beforeEachTestMethod() {
        System.out.println("Executed before each test");
    }

    @Ignore("Why did we ignore this?")
    @Test
    public void testSomethingIsIgnored() {
        System.out.println("Don't execute this test");
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("Executed after each class");
    }

    @After
    public void afterEachTestMethod() {
        System.out.println("Executed after each test");
    }

}
