package com.javafortesters.arrays;

import junit.framework.TestCase;
import org.junit.Test;

public class ArrayPrinterTest {

    @Test
    public void testPrint2DIntArrayArrayDefinition() {
        ArrayPrinter arrayPrinter = new ArrayPrinter();

        int[][] multiArray = new int[2][2];

        multiArray[0][0] = 1;
        multiArray[0][1] = 2;
        multiArray[1][0] = 3;
        multiArray[1][1] = 4;

        arrayPrinter.print2DIntArray(multiArray);
    }

    @Test
    public void testPrint2DIntArrayQuickArrayDefinition() {
        ArrayPrinter arrayPrinter = new ArrayPrinter();

        int[][] multiArray = {{1, 2}, {3, 4}};

        arrayPrinter.print2DIntArray(multiArray);
    }


    @Test
    public void testPrint2DIntArrayClassic() {
        ArrayPrinter arrayPrinter = new ArrayPrinter();

        int[][] multiArray = {{1, 2}, {3, 4}};

        arrayPrinter.print2DIntArrayClassic(multiArray);
    }

    @Test
    public void testPrint2DIntArrayClassicBonus() {
        ArrayPrinter arrayPrinter = new ArrayPrinter();

        int[][] multiArray = {{1, 2}, {3, 4}};

        arrayPrinter.print2DIntArrayClassicBonus(multiArray);
    }


}