package com.javafortesters.asserts;

import org.junit.Test;

import static org.junit.Assert.*;

public class AssertTest {

    @Test
    public void testAssertEquals() {
        assertEquals("Waarde", "Waarde");
    }

    @Test
    public void testAssertFalse() {
        assertFalse(false);
    }

    @Test
    public void testAssertTrue() {
        assertTrue(true);
    }

    @Test
    public void testAssertArrayEquals() {
        int[] intArray = new int[]{1, 2, 3};
        assertArrayEquals(new int[]{1, 2, 3}, intArray);
        assertNotEquals(new int[]{1, 2, 3}, intArray);
    }

    @Test
    public void testAssertNotNull() {
        assertNotNull("Waarde");
    }

    @Test
    public void testAssertNotSame() {
        String a = "a";
        String b = "b";
        assertNotSame(a,b);
    }

    @Test
    public void testAssertNull() {
        assertNull(null);
    }

    @Test
    public void testAssertSame() {
        String a = "a";
        assertSame(a,a);
    }
}
