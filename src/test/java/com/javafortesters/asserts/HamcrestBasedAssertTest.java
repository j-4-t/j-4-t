package com.javafortesters.asserts;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;

public class HamcrestBasedAssertTest {

    @Test
    public void testAssertEquals() {
        assertThat("Waarde", is("Waarde"));
    }

    @Test
    public void testAssertFalse() {
        assertThat(false, is(false));
    }

    @Test
    public void testAssertTrue() {
        assertThat(true, is(true));
    }

    @Test
    public void testAssertArrayEquals() {
        int[] intArray = new int[]{1, 2, 3};
       assertThat(new int[] { 1,2,3}, equalTo(intArray));
    }

    @Test
    public void testAssertNotNull() {
        assertNotNull("Waarde");
        assertThat("Waarde", is(not(nullValue())));
        assertThat("Waarde", is(notNullValue()));
    }

    @Test
    public void testAssertNotSame() {
        String a = "a";
        String b = "b";
        assertThat(a, is(not(b)));
    }

    @Test
    public void testAssertNull() {

        assertThat(null, is(nullValue()));
    }

    @Test
    public void testAssertSame() {
        assertThat("a", is("a"));
    }

    @Test
    public void testContainsString() {
        assertThat("Boek",containsString("oe"));
    }
    @Test
    public void testEndsWith() {
        assertThat("Boek", endsWith("oek"));

    }

    @Test
    public void testStartsWith() {
        assertThat("Boek", startsWith("Boe"));
    }

}
