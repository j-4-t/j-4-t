package com.javafortesters.collections.days;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DeterminePositionOfMondayTest {

    List<String> days;

    @Before
    public void initDays() {
        String[] someDays = {"Tuesday", "Thursday",
                "Wednesday", "Monday",
                "Saturday", "Sunday",
                "Friday"};
        days = Arrays.asList(someDays);
    }

    @Test
    public void usingAForEachLoopAndBreak() {
        int forCount = 0;
        for (String day : days) {
            if (day.equals("Monday")) {
                break;
            }
            forCount++;
        }
        assertEquals("Monday is at position 3", 3, forCount);
    }

    @Test
    public void usingAForLoopAndBreak() {
        int loopCount;
        for (loopCount = 0; loopCount <= days.size(); loopCount++) {
            if (days.get(loopCount).equals("Monday")) {
                break;
            }
        }
        assertEquals("Monday is at position 3", 3, loopCount);
    }

    @Test
    public void usingAWhileLoop() {
        int count = 0;
        while (!days.get(count).equals("Monday")) {
            count++;
        }
        assertEquals("Monday is at position 3", 3, count);
    }

    @Test
    public void usingADoWhileLoop() {
        int docount = -1;
        do {
            docount++;
        } while (!days.get(docount).equals("Monday"));
        assertEquals("Monday is at position 3", 3, docount);
    }

    @Test
    public void usingAForLoopWithWhileCondition() {

        int doCount;
        for (doCount = 0; !days.get(doCount).equals("Monday"); doCount++) {
        }
        assertEquals("Monday is at position 3", 3, doCount);
    }
}
