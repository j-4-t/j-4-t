package com.javafortesters.datetime;


import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class DateTimeTest {

    @Test
    public void TestCurrentTimeMillis() {
        long startTime = System.currentTimeMillis();
        for (int x = 0; x < 10; x++) {
            System.out.println("Current Time " +
                    System.currentTimeMillis());
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Total Time " + (endTime - startTime));
    }

    @Test
    public void TestCurrentTimeNanos() throws InterruptedException {
        long startTime = System.nanoTime();
        for (int x = 0; x < 10; x++) {
            System.out.println("Current Time " +
                    System.nanoTime());
        }
        long endTime = System.nanoTime();
        System.out.println("Total Time " + (endTime - startTime));
    }

    @Test
    public void createAUniqueUserIDAllChars() {
        StringBuilder userId = new StringBuilder("user_");
        long currentTime = System.currentTimeMillis();
        while (currentTime > 0) {
            userId.append((char) ('a' + (currentTime % 10)));
            currentTime = currentTime / 10;
        }

        List<Character> digits = new ArrayList<>();
        assertThat(userId.toString(), not(containsString("0")));
        assertThat(userId.toString(), not(containsString("1")));
        assertThat(userId.toString(), not(containsString("2")));
        assertThat(userId.toString(), not(containsString("3")));
        assertThat(userId.toString(), not(containsString("4")));
        assertThat(userId.toString(), not(containsString("5")));
        assertThat(userId.toString(), not(containsString("6")));
        assertThat(userId.toString(), not(containsString("7")));
        assertThat(userId.toString(), not(containsString("8")));
        assertThat(userId.toString(), not(containsString("9")));
        assertThat(userId.toString(), not(containsString("0")));

        System.out.println(userId.toString());
    }

    @Test
    public void printToStringOfCalendarToConsole() {
        Calendar calendar = Calendar.getInstance();
        System.out.println(calendar.toString());
    }

    @Test
    public void getCalendarDetails(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(2013, Calendar.DECEMBER, 15, 23, 39, 54);
        assertThat(calendar.get(Calendar.MONTH), is(Calendar.DECEMBER));
        assertThat(calendar.get(Calendar.YEAR), is(2013));
        assertThat(calendar.get(Calendar.DAY_OF_MONTH), is(15));
        assertThat(calendar.get(Calendar.HOUR_OF_DAY), is(23));
        assertThat(calendar.get(Calendar.MINUTE), is(39));
        assertThat(calendar.get(Calendar.HOUR), is(11));
        assertThat(calendar.get(Calendar.AM_PM), is(Calendar.PM));
    }

    @Test
    public void getExperimentWithCalendarDetails(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(2013, Calendar.DECEMBER, 15, 23, 39, 54);
        assertThat(calendar.get(Calendar.DAY_OF_WEEK), is(1));
        assertThat(calendar.get(Calendar.DAY_OF_WEEK), is(Calendar.SUNDAY));
        assertThat(calendar.get(Calendar.WEEK_OF_MONTH), is(3));
        assertThat(calendar.get(Calendar.WEEK_OF_YEAR), is(51));
        assertThat(calendar.get(Calendar.DAY_OF_YEAR), is(349));
    }

    @Test
    public void incrementAndDecrementOtherFields(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(2013, Calendar.DECEMBER, 15, 23, 39, 54);
        calendar.add(Calendar.YEAR, -2);
        calendar.add(Calendar.MONTH, -6);
        calendar.add(Calendar.DAY_OF_MONTH, -12);
        assertThat(calendar.get(Calendar.YEAR), is(2011));
        assertThat(calendar.get(Calendar.MONTH), is(Calendar.JUNE));
        assertThat(calendar.get(Calendar.DAY_OF_MONTH), is(3));
        calendar.set(2013, Calendar.DECEMBER, 15, 23,39, 54);

        calendar.add(Calendar.DAY_OF_MONTH, 19);
        calendar.add(Calendar.MONTH, 5);
        calendar.add(Calendar.YEAR, -3);
        assertThat(calendar.get(Calendar.YEAR), is(2011));
        assertThat(calendar.get(Calendar.MONTH), is(Calendar.JUNE));
        assertThat(calendar.get(Calendar.DAY_OF_MONTH), is(3));
    }

    @Test
    public void rollCalendar(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(2013, Calendar.DECEMBER, 15, 23, 39, 54);
        calendar.roll(Calendar.DAY_OF_MONTH, 17);
        assertThat(calendar.get(Calendar.YEAR), is(2013));
        assertThat(calendar.get(Calendar.MONTH), is(Calendar.DECEMBER));
        assertThat(calendar.get(Calendar.DAY_OF_MONTH), is(1));
        calendar.set(2013, Calendar.DECEMBER, 15, 23,39, 54);
        calendar.add(Calendar.DAY_OF_MONTH,17);
        assertThat(calendar.get(Calendar.YEAR), is(2014));
        assertThat(calendar.get(Calendar.MONTH), is(Calendar.JANUARY));
        assertThat(calendar.get(Calendar.DAY_OF_MONTH), is(1));
    }
}

