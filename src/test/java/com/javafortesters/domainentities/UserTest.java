package com.javafortesters.domainentities;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

import org.junit.Test;

/**
 * Created by rb on 17-10-14.
 */
public class UserTest {
	
	@Test
	public void testCanConstructANewUser() {
		User user = new User();
		assertNotNull(user);
	}
	
	@Test
	public void testUserHasDefaultUsernameAndPassword() {
		User user = new User();
		
		assertEquals("password", user.getPassword());
		assertEquals("username", user.getUsername());
	}

    @Test
    public void testCanConstructANewUserWithUsernameAndPassword() {
        User user = new User("myUsername", "myPassword");

        assertEquals("myPassword", user.getPassword());
        assertEquals("myUsername", user.getUsername());
    }

    @Test
    public void testSetPasswordAfterConstructed() {
        User user = new User();
        user.setPassword("P@ssw0rd!");
        assertEquals("P@ssw0rd!", user.getPassword());
    }

    @Test(expected = IllegalPasswordException.class)
    public void testSetPasswordThatsNotValid() {
        User user = new User();
        user.setPassword("password");
    }

    @Test
    public void testSetPasswordThatsValid() {
        User user = new User();
        user.setPassword("P@ssw0rd!");
        assertEquals("P@ssw0rd!", user.getPassword());
    }
}
