package com.javafortesters.exceptions;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExceptionTest {

    @Test(expected = NullPointerException.class)
    public void throwANullPointerException() {
        Integer age = null;
        String ageAsString = age.toString();
        String yourAge = "You are " + ageAsString + " years old";
        assertEquals("You are 18 years old", yourAge);
    }

    @Test
    public void fixedThrowANullPointerException() {
        Integer age = 18;
        String ageAsString = age.toString();
        String yourAge = "You are " + ageAsString + " years old";
        assertEquals("You are 18 years old", yourAge);
    }

    @Test(expected = NullPointerException.class)
    public void catchAnArithmeticExceptionInsteadOfNullPointerException() {
        Integer age = null;
        String ageAsString;
        try {
            ageAsString = age.toString();
        } catch (ArithmeticException e) {
            age = 18;
            ageAsString = age.toString();
        }
        String yourAge = "You are " + age.toString() + " years old";
        assertEquals("You are 18 years old", yourAge);
    }

   @Test(expected = NullPointerException.class)
    public void triggerNullPointerExceptionFromCatchBlock() {
        Integer age = null;
        String ageAsString;
        try {
            ageAsString = age.toString();
        } catch (ArithmeticException e) {
            ageAsString = age.toString();
        }
        String yourAge = "You are " + age.toString() + " years old";
        assertEquals("You are 18 years old", yourAge);
    }


    // Commented out because this won't compile
    /*
    @Test
    public void catchingACheckedExceptionThatsNotThrownWontCompile() {
        Integer age = null;
        String ageAsString;
        try {
            ageAsString = age.toString();
        } catch (NoSuchMethodException e) {
            ageAsString = age.toString();
        }
        String yourAge = "You are " + age.toString() + " years old";
        assertEquals("You are 18 years old", yourAge);
    }
    */


    @Test
    public void useExceptionAsAnObject() {
        Integer age = null;

        try {
            age.toString();
        } catch (NullPointerException e) {
            System.out.println("getMessage - " + e.getMessage());
            System.out.println("getStacktrace - " + e.getStackTrace());
            System.out.println("printStackTrace");
            e.printStackTrace();
        }
    }
}
