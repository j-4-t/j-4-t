package com.javafortesters.propertyfiles;

import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PropertyFilesTest {

    @Test
    public void canReadAPropertiesFileInputStream() throws IOException {
        String workingDirectory = System.getProperty("user.dir");
        String resourceFilePath = workingDirectory +
                "/src/test/resources/" +
                "property_files/" +
                "static_example.properties";
        Properties sample = new Properties();
        FileInputStream propertyFileInputStream = new
                FileInputStream(resourceFilePath);
        try {
            sample.load(propertyFileInputStream);

            assertThat(sample.size(), is(7));
        } finally {
            propertyFileInputStream.close();
        }
    }

    @Test
    public void simpleSavePropertiesFile() throws IOException {
        // Fixed and improved file handling
        File tempDirectory = new File(System.getProperty("java.io.tmpdir"));
        String tempResourceFilePath = new File(tempDirectory, "tempFileForPropertiesStoreTest.properties").getAbsolutePath();

        Properties saved = new Properties();
        saved.setProperty("prop1", "Hello");
        saved.setProperty("prop2", "World");
        FileOutputStream outputFile = new FileOutputStream(tempResourceFilePath);
        saved.store(outputFile, "Hello There World");
        outputFile.close();
        FileReader propertyFileReader = new FileReader(tempResourceFilePath);
        Properties loaded = new Properties();
        try {
            loaded.load(propertyFileReader);
        } finally {
            propertyFileReader.close();
        }
        assertThat(loaded.getProperty("prop1"), is("Hello"));
        assertThat(loaded.getProperty("prop2"), is("World"));
    }
}
