package com.javafortesters.random;


import org.junit.Test;

import java.util.*;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class RandomTest {

    @Test
    public void testThatGeneratedRandomIntIsBetweenLimits() {
        Random generate = new Random();
        for (int x = 0; x < 1000; x++) {
            int randomInt = generate.nextInt();
            System.out.println(randomInt);
            assertThat(randomInt < Integer.MAX_VALUE, is(true));
            assertThat(randomInt >= Integer.MIN_VALUE, is(true));
        }
    }

    @Test
    public void testThatGeneratedRandomIntIsWithInRange() {
        Set<Integer> result = new HashSet<>();
        Random generate = new Random();

        int minValue = 15;
        int maxValue = 20;

        for (int x = 0; x < 1000; x++) {
            result.add(generate.nextInt(maxValue - minValue + 1 + minValue));
        }

        assertTrue(result.contains(15));
        ;
        assertTrue(result.contains(16));
        ;
        assertTrue(result.contains(17));
        ;
        assertTrue(result.contains(18));
        ;
        assertTrue(result.contains(19));
        ;
        assertTrue(result.contains(20));
        ;

    }

    @Test
    public void testGenerationOfRandomGaussianDistributionDouble() {
        Random generate = new Random();
        int standardDeviationCount1 = 0;
        int standardDeviationCount2 = 0;
        int standardDeviationCount3 = 0;
        int standardDeviationCount4 = 0;
        for (int x = 0; x < 1000; x++) {
            double randomGaussian = generate.nextGaussian();
//System.out.println(randomValue);
            if (randomGaussian > -1.0d && randomGaussian < 1.0d)
                standardDeviationCount1++;
            if (randomGaussian > -2.0d && randomGaussian < 2.0d)
                standardDeviationCount2++;
            if (randomGaussian > -3.0d && randomGaussian < 3.0d)
                standardDeviationCount3++;
            if (randomGaussian > -4.0d && randomGaussian < 4.0d)
                standardDeviationCount4++;
        }
        float sd1percentage = (standardDeviationCount1 / 1000f) * 100f;
        System.out.println("about 70% one standard deviation = " + sd1percentage);
        float sd2percentage = (standardDeviationCount2 / 1000f) * 100f;
        System.out.println("about 95% two standard deviation = " + sd2percentage);
        float sd3percentage = (standardDeviationCount3 / 1000f) * 100f;
        System.out.println("about 99% three standard deviation = " + sd3percentage);
        float sd4percentage = (standardDeviationCount4 / 1000f) * 100f;
        System.out.println("about 99.9% four standard deviation = " + sd4percentage);
    }

    @Test
    public void canGenerateAgeUsingDeviation() {
        Random generate = new Random();
        Map<Integer, Integer> ages = new HashMap<>();
        for (int x = 0; x < 1000; x++) {
            int age = (int) (generate.nextGaussian() * 5) + 35;
            int ageCount = 0;
            if (ages.containsKey(age)) {
                ageCount = ages.get(age);
            }
            ageCount++;
            ages.put(age, ageCount);
        }
        SortedSet<Integer> agesSorted = new TreeSet(ages.keySet());
        for (int age : agesSorted) {
            System.out.println(age + " : " + ages.get(age));
        }
    }

    @Test
    public void canGenerateRandomNumbersWithSeed() {
        for (int x = 0; x < 10; x++) {
            Random generate = new Random(1234567L);
            assertThat(generate.nextInt(), is(1042961893));
            assertThat(generate.nextLong(), is(-6749250865724111202L));
            assertThat(generate.nextDouble(), is(0.44762832574617084D));
            assertThat(generate.nextGaussian(), is(-0.11571220872310763D));
            assertThat(generate.nextFloat(), is(0.33144182F));
            assertThat(generate.nextBoolean(), is(false));
        }
    }

    @Test
    public void generateARandomString() {
        String validValues = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
        StringBuilder rString;
        Random random = new Random();
        rString = new StringBuilder();
        for (int x = 0; x < 100; x++) {
            char rChar = validValues.charAt(random.nextInt(validValues.length()));
            rString.append(rChar);
        }
        System.out.println(rString.toString());
    }
}
