package com.javafortesters.stringbuilders;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class StringBuilderTest {

    @Test
    public void testResizeWhenOverInitialCapacity() {

        StringBuilder stringBuilder = new StringBuilder(10);
        assertEquals(10, stringBuilder.capacity());

        stringBuilder.append("Something bigger then initial capacity");

        assertTrue(stringBuilder.capacity() > 10);

    }

    @Test
    public void testInsertInEmtyStringBuilder() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.insert(0, "test");

        assertEquals("test", stringBuilder.toString());
    }

    @Test
    public void testInsertAtTheEndOfStringBuilder() {
        StringBuilder stringBuilder = new StringBuilder("HelloWorld");

        stringBuilder.insert(stringBuilder.length(), "test");

        assertEquals("HelloWorldtest", stringBuilder.toString());
    }

    @Test
    public void testInsertAtTheMiddleOfStringBuilder() {
        StringBuilder stringBuilder = new StringBuilder("HelloWorld");
        stringBuilder.insert(stringBuilder.length()/2, "test");
        assertEquals("HellotestWorld", stringBuilder.toString());
    }

    @Test
    public void testInsertAtTheMiddleOfStringBuilderWithOddLength() {
        StringBuilder stringBuilder = new StringBuilder("HelloWorld!");
        stringBuilder.insert(stringBuilder.length()/2, "test");
        assertEquals("HellotestWorld!", stringBuilder.toString());
    }
}
