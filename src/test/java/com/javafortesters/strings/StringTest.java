package com.javafortesters.strings;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class StringTest {

    @Test
    public void testEscapeCharacters() {
        System.out.println(":\t:");
        System.out.println(":\b:");
        System.out.println(":\n:");
        System.out.println(":\r:");
        System.out.println(":\':");
        System.out.println(":\":");
        System.out.println(":\\:");
    }

    @Test
    public void testStringConstruction() {
        String fromString = new String("String");
        String fromCharArray = new String(new char[] {'S','t','r','i','n','g'});
        String fromByteArray = new String(new byte[] { 83, 116, 114, 105, 110, 103});

        assertEquals(fromString, fromCharArray);
        assertEquals(fromString, fromByteArray);
    }

    @Test
    public void testRegionMatches() {
        String helloFella = "Hello fella";
        String youngLady = "young lady";
        assertTrue(helloFella.regionMatches(true, 9, youngLady, 6, 2));
    }
}
