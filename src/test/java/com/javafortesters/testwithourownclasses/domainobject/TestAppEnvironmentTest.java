package com.javafortesters.testwithourownclasses.domainobject;

import static junit.framework.TestCase.assertEquals;

import org.junit.Test;

import com.javafortesters.domainobject.TestAppEnv;

/**
 * Created by rb on 17-10-14.
 */
public class TestAppEnvironmentTest {
	
	@Test
	public void testCanGetUrlStatically() {
		assertEquals("Returns Hard Coded URL", "http://192.123.0.3:67", TestAppEnv.getUrl());
	}
	
	@Test
	public void testCanGetDomainStatically() {
		assertEquals("Just the Domain", "192.123.0.3", TestAppEnv.DOMAIN);
	}
	
	@Test
	public void testCanGetPortStatically() {
		assertEquals("Just the Port", "67", TestAppEnv.PORT);
	}

    @Test
    public void testCanGetProtocolStatically() {
        assertEquals("Just the protocol", "http://", TestAppEnv.PROTOCOL);


    }
}
