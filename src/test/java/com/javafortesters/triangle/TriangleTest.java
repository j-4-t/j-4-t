package com.javafortesters.triangle;

import org.junit.Test;

public class TriangleTest {

    @Test
    public void testPrintingOfTriangle() {
        TriangleGenerator triangleGenerator = new TriangleGenerator();
        int[][] triangle = triangleGenerator.generateTriangle(15);
        print2DIntArray(triangle);
    }

    @Test
    public void testPrintingOfSymmetricTriangle() {
        TriangleGenerator triangleGenerator = new TriangleGenerator();
        int[][] triangle = triangleGenerator.generateSymmetricTriangle(15);
        print2DIntArray(triangle);
    }

    private void print2DIntArray(int[][] multi) {
        for (int[] outer : multi) {
            if (outer == null) {
                System.out.print("null");
            } else {
                for (int inner : outer) {
                    System.out.print(inner + ",");
                }
            }
            System.out.println("");
        }
    }
}