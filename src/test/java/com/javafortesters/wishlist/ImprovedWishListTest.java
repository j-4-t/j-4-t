package com.javafortesters.wishlist;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Date;

import static junit.framework.TestCase.fail;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;


public class ImprovedWishListTest {

    private final static Date OUR_TEST_DATE = new Date();

    /*
    This unit tests verifies your implementation of our ImprovedWishList.
    Execute the tests and notice that the test fails because it there are no active test.

    After that start activating (and fixing) the test one by one by moving the start comment tag bellow the test.

    Note : We are using the Hamcrest matchers instead of the default JUnit Asserts

    Hint : Use your IntelliJ IDEA Shortcuts (for example: ALT + ENTER, ALT + INSERT) to quickly generate the
    required classes, methods, toString, hashCode, equals, etc.
    */


    /* <- TODO Move below the test, fix it and continue until all tests are fixed

    @Test
    public void verifyThatWeCanInstantiateADonation() {
        Donation donation = new Donation();
        assertThat(donation, is(notNullValue()));

    }



    @Test
    public void verifyThatDonationExtendsGift() {
        Donation donation = new Donation();
        assertThat(donation, is(instanceOf(Gift.class)));
    }

    @Test
    public void verifyThatWeCanSetAndGetTheBeneficiaryOfADonation() {
        Donation donation = new Donation();
        donation.setBeneficiary("Red Cross");
        assertThat(donation.getBeneficiary(), is("Red Cross"));
    }

    // This test is used for verification only and should not lead to any changes if you added beneficiary as part of Donation

    @Test(expected = NoSuchMethodException.class)
    public void verifyThatTheBeneficiaryWasNotAddedToGiftByAccident() throws NoSuchMethodException {
        assertThat(Gift.class.getDeclaredMethod("setBeneficiary", String.class), is(nullValue()));
    }

    @Test
    public void verifyThatTheDefaultConstructorOfDonationCallsTheDefaultsConstructorOfGift() {
        Donation donation = new Donation();
        assertThat(donation.getName(), is("Surprise"));
    }

    @Test
    public void verifyThatConstructorOfDonationAcceptsANameAndBeneficiary() {
        Donation donation = new Donation("Donation to the Red Cross", "Red Cross");
        assertThat(donation.getName(), is("Donation to the Red Cross"));
        assertThat(donation.getBeneficiary(), is("Red Cross"));
    }

    @Test
    public void verifyThatConstructorOfDonationAcceptsOnlyABeneficiary() {
        Donation donation = new Donation("Red Cross");
        assertThat(donation.getName(), is("Surprise"));
        assertThat(donation.getBeneficiary(), is("Red Cross"));
    }


    @Test
    public void verifyThatTheToStringMethodOfABeneficiaryIsImplemented() {
        Donation donation = new Donation("Donation to the Red Cross", "Red Cross");
        assertThat(donation.toString(), is("Donation{name='Donation to the Red Cross', beneficiary='Red Cross'}"));
    }



// We are going to print the wishlist to System.out, please:

//        * Create the WishListPrinter class and add a static methods to print the parts of the wishlist
//        * Use a StringBuilder to create the desired output
//        * Finally print the contents of the StringBuilder using System.out.println


    @Test
    public void shouldBeAbleToPrintTheWishListHeaderToSystemOutPrintln() {
        // Catch everything printed to System.out
        OutputStreamCatcher osCatcher = new OutputStreamCatcher();

        WishList wishList = buildWishListForPrint();
        WishListPrinter.printHeader(wishList);

        // Retrieve everything that's printed to System.out and restore the initial System.out
        String output = osCatcher.getContentAsString();

        System.out.println(output);

        String[] lines = output.split("\n");
        int l = 0;

        assertThat(lines[l++], is("--------------------------------------------------"));
        assertThat(lines[l++], is("MY WISHLIST:"));
        assertThat(lines[l++], is("--------------------------------------------------"));
    }


    // Hint : use String.format to print the price using 2 decimals
    @Test
    public void shouldBeAbleToPrintTheWishListGiftsToSystemOutPrintln() {
        // Catch everything printed to System.out
        OutputStreamCatcher osCatcher = new OutputStreamCatcher();

        WishList wishList = buildWishListForPrint();
        WishListPrinter.printGifts(wishList);

        // Retrieve everything that's printed to System.out and restore the initial System.out
        String output = osCatcher.getContentAsString();

        System.out.println(output);

        String[] lines = output.split("\n");
        int l = 0;

        assertThat(lines[l++], is("1.\tBall\t:\tNice orange ball\t:\t10.00"));
        assertThat(lines[l++], is("2.\tPencil\t:\t<no description>\t:\t1.00"));
        assertThat(lines[l++], is("3.\tBike\t:\t34 inch frame size\t:\t110.00"));
    }

    @Test
    public void shouldBeAbleToPrintTheWishListTotalsToSystemOutPrintln() {
        // Catch everything printed to System.out
        OutputStreamCatcher osCatcher = new OutputStreamCatcher();

        WishList wishList = buildWishListForPrint();
        WishListPrinter.printTotals(wishList);

        // Retrieve everything that's printed to System.out and restore the initial System.out
        String output = osCatcher.getContentAsString();

        System.out.println(output);

        String[] lines = output.split("\n");
        int l = 0;


        assertThat(lines[l++], is("--------------------------------------------------"));
        assertThat(lines[l++], is("TOTALS:"));
        assertThat(lines[l++], is("--------------------------------------------------"));
        assertThat(lines[l++], is("Total gifts:\t3 of max. 10"));
        assertThat(lines[l++], is("Total amount:\t121.00"));

    }

    @Test
    public void shouldBeAbleToPrintTheWishListUsingSystemOutPrintln() {
        // Catch everything printed to System.out
        OutputStreamCatcher osCatcher = new OutputStreamCatcher();

        WishList wishList = buildWishListForPrint();
        WishListPrinter.print(wishList);

        // Retrieve everything that's printed to System.out and restore the initial System.out
        String output = osCatcher.getContentAsString();

        System.out.println(output);

        String[] lines = output.split("\n");
        int l = 0;

        assertThat(lines[l++], is("--------------------------------------------------"));
        assertThat(lines[l++], is("MY WISHLIST:"));
        assertThat(lines[l++], is("--------------------------------------------------"));
        assertThat(lines[l++], is("1.\tBall\t:\tNice orange ball\t:\t10.00"));
        assertThat(lines[l++], is("2.\tPencil\t:\t<no description>\t:\t1.00"));
        assertThat(lines[l++], is("3.\tBike\t:\t34 inch frame size\t:\t110.00"));
        assertThat(lines[l++], is("--------------------------------------------------"));
        assertThat(lines[l++], is("TOTALS:"));
        assertThat(lines[l++], is("--------------------------------------------------"));
        assertThat(lines[l++], is("Total gifts:\t3 of max. 10"));
        assertThat(lines[l++], is("Total amount:\t121.00"));
        assertThat(lines[l++], is("--------------------------------------------------"));
    }

    @Test
    public void shouldFormatTheGiftNameToUpperCaseEachWord() {
         // Note that we do not use this function while printing the list because this would complicate all other wishlist printing tests

        assertThat(WishListPrinter.formatGiftName("Nice orange ball"), is("Nice Orange Ball"));
        assertThat(WishListPrinter.formatGiftName("NICE orange ball"), is("Nice Orange Ball"));
        assertThat(WishListPrinter.formatGiftName("niCE orangE ball"), is("Nice Orange Ball"));
    }

    @Test
    public void canRetrieveRandomGiftFromWishList() {
        WishList wishList = buildWishListForPrint();

        // Note : Please seed the Random generator in the wishlist with the following value: 1234567890
        assertThat(wishList.getRandomGift().getName(), is("Ball"));
        assertThat(wishList.getRandomGift().getName(), is("Bike"));
        assertThat(wishList.getRandomGift().getName(), is("Bike"));
        assertThat(wishList.getRandomGift().getName(), is("Ball"));
        assertThat(wishList.getRandomGift().getName(), is("Bike"));
        assertThat(wishList.getRandomGift().getName(), is("Bike"));
        assertThat(wishList.getRandomGift().getName(), is("Bike"));
        assertThat(wishList.getRandomGift().getName(), is("Bike"));
        assertThat(wishList.getRandomGift().getName(), is("Pencil"));
    }

    @Test
    public void canRetrieveGiftsInPriceRange() {
        WishList wishList = buildWishListForPrint();

        // Should return Pencil
        assertThat(wishList.getGiftsInPriceRange(BigDecimal.ONE, BigDecimal.ONE).size(), is(1));
        assertThat(wishList.getGiftsInPriceRange(BigDecimal.ZERO, BigDecimal.ONE).size(), is(1));

        // Should return nothing

        assertThat(wishList.getGiftsInPriceRange(BigDecimal.ZERO, BigDecimal.ZERO).size(), is(0));

        // Should return Pencil and Ball
        assertThat(wishList.getGiftsInPriceRange(BigDecimal.ONE, BigDecimal.TEN).size(), is(2));

        // Should return nothing because of invalid range
        assertThat(wishList.getGiftsInPriceRange(BigDecimal.TEN, BigDecimal.ONE).size(), is(0));


        // Should return Ball
        assertThat(wishList.getGiftsInPriceRange(BigDecimal.TEN, BigDecimal.TEN).size(), is(1));

        // Should return everything
        assertThat(wishList.getGiftsInPriceRange(BigDecimal.ZERO, new BigDecimal(110.0)).size(), is(3));
    }

    @Test
    public void canRetrieveRandomGiftFromWishListWithinPriceRange() {
        WishList wishList = buildWishListForPrint();

        // Note : Please seed the Random generator in the wishlist with the following value: 1234567890
        assertThat(wishList.getRandomGift(BigDecimal.ONE, new BigDecimal(15.0)).getName(), is("Pencil"));
        assertThat(wishList.getRandomGift(BigDecimal.ONE, new BigDecimal(15.0)).getName(), is("Pencil"));
        assertThat(wishList.getRandomGift(BigDecimal.ONE, new BigDecimal(15.0)).getName(), is("Pencil"));
        assertThat(wishList.getRandomGift(BigDecimal.ONE, new BigDecimal(15.0)).getName(), is("Pencil"));
        assertThat(wishList.getRandomGift(BigDecimal.ONE, new BigDecimal(15.0)).getName(), is("Pencil"));
        assertThat(wishList.getRandomGift(BigDecimal.ONE, new BigDecimal(15.0)).getName(), is("Pencil"));
        assertThat(wishList.getRandomGift(BigDecimal.ONE, new BigDecimal(15.0)).getName(), is("Ball"));
        assertThat(wishList.getRandomGift(BigDecimal.ONE, new BigDecimal(15.0)).getName(), is("Pencil"));
        assertThat(wishList.getRandomGift(BigDecimal.ONE, new BigDecimal(15.0)).getName(), is("Pencil"));
    }



    @Test
    public void shouldIgnoreThisTestBecauseItAlwaysFails() {
        fail();
    }

    @Test
    public void shouldSpecifyExpectedExceptionUsingExpected() {
        WishList wishList = new WishList();
        wishList.setMaximumGifts(1);
        wishList.addGift(new Gift());
        wishList.addGift(new Gift());
    }

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @Test
    public void shouldSpecifyExpectedExceptionUsingRuleToVerifyErrorMessage() throws WishListFullException {

       // Use the expected object defined by @Rule here

        WishList wishList = new WishList();
        wishList.setMaximumGifts(1);
        wishList.addGift(new Gift());
        wishList.addGift(new Gift());
    }






  */

    /*
    Helper methods and classes for our tests
     */


    private WishList buildWishListForPrint() {
        Gift ball = new Gift("Ball");
        ball.setDescription("Nice orange ball");
        ball.setPrice(new BigDecimal(10.0));

        Gift otherBall = new Gift("Pencil");
        otherBall.setPrice(new BigDecimal(1.0));

        Gift bike = new Gift("Bike");
        bike.setDescription("34 inch frame size");
        bike.setPrice(new BigDecimal(110.0));

        WishList wishList = new WishList("My WishList");
        wishList.setMaximumGifts(10);
        wishList.addGift(ball);
        wishList.addGift(otherBall);
        wishList.addGift(bike);

        return wishList;
    }

    private class OutputStreamCatcher {

        private final PrintStream currentOutputStream;

        private final ByteArrayOutputStream outContent;

        // Catch everything printed to System.out
        public OutputStreamCatcher() {
            outContent = new ByteArrayOutputStream();
            currentOutputStream = System.out;
            System.setOut(new PrintStream(outContent));
        }

        // Retrieve everything that's printed to System.out and restore the initial System.out
        public String getContentAsString() {
            System.setOut(currentOutputStream);
            return outContent.toString();
        }
    }

}
