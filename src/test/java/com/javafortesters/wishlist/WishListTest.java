package com.javafortesters.wishlist;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import static org.junit.Assert.*;


public class WishListTest {

    private final static Date OUR_TEST_DATE = new Date();

    /*
    This unit tests verifies your implementation of our WishList.
    Execute the tests and notice that the test fails because it there are no active test.

    After that start activating (and fixing) the test one by one by moving the start comment tag bellow the test.

    Hint : Use your IntelliJ IDEA Shortcuts (for example: ALT + ENTER, ALT + INSERT) to quickly generate the
    required classes, methods, toString, hashCode, equals, etc.
    */



    @Test
    public void verifyThatWeCanInstantiateAGift() {
        Gift gift = new Gift();
        assertNotNull(gift);

    }

    @Test
    public void verifyThatWeCanSetAndGetNameTheNameOfAGift() {
        Gift gift = new Gift();
        gift.setName("Bike");
        assertEquals("Bike", gift.getName());
    }

    @Test
    public void verifyThatWeCanSetAndGetThePriceOfAGift() {
        Gift gift = new Gift();
        gift.setPrice(new BigDecimal(1.25));
        assertEquals(new BigDecimal(1.25), gift.getPrice());
    }

    @Test
    public void verifyThatWeCanSetAndGetTheDescriptionOfAGift() {
        Gift gift = new Gift();
        gift.setDescription("Nice one!");
        assertEquals("Nice one!", gift.getDescription());
    }

    @Test
    public void verifyThatWeCanSetAndGetTheDateWishedOfGift() {
        Gift gift = new Gift();
        gift.setWishedDate(OUR_TEST_DATE);
        assertEquals(OUR_TEST_DATE, gift.getWishedDate());
    }

    @Test
    public void verifyThatWecanInstantiateAGiftWithAConstructorThatSetsTheName() {
        Gift gift = new Gift("Ball");
        assertEquals("Ball", gift.getName());
    }

    @Test
    public void verifyThatTheDefaultConstructorSetsTheCorrectDefaults() {
        // Hint : Use Constants to store the default values
        Gift gift = new Gift();

        assertEquals("Surprise", gift.getName());
        assertEquals(BigDecimal.ZERO, gift.getPrice());
        assertNull(gift.getDescription());
        assertNotNull(gift.getWishedDate());
    }

    @Test
    public void verifyThatTheConstructorWithNameSetsTheCorrectDefaults() {
        // Hint : Use Constants to store the default values

        Gift gift = new Gift("Ball");

        assertEquals("Ball", gift.getName());
        assertEquals(BigDecimal.ZERO, gift.getPrice());
        assertNull(gift.getDescription());
        assertNotNull(gift.getWishedDate());
    }

    @Test
    public void verifyThatTheToStringMethodOfAGiftIsImplemented() {
        Gift gift = new Gift("Ball");
        assertEquals("Gift{name='Ball'}", gift.toString());
    }

    @Test
    public void verifyThatTheEqualsAndHashCodeOfAGiftAreImplementedBasedOnNameAndPrice() {
        // Hint : generate it using IDEA (alt + insert)

        Gift ball = new Gift("Ball");
        ball.setPrice(new BigDecimal(10.0));

        Gift otherBall = new Gift("Ball");
        otherBall.setPrice(new BigDecimal(10.0));

        Gift bike = new Gift("Bike");
        bike.setPrice(new BigDecimal(110.0));

        Gift pricelessBall = new Gift("Ball");
        Gift pricelessBike = new Gift("Bike");

        assertTrue(ball.equals(ball));
        assertTrue(ball.equals(otherBall));
        assertFalse(ball.equals(bike));
        assertFalse(ball.equals(pricelessBall));
        assertFalse(bike.equals(pricelessBike));
        assertFalse(bike.equals(null));
    }

    @Test
    public void verifyThatWeCanInstantiateAWishList() {
        WishList wishList = new WishList();
        assertNotNull(wishList);
    }

    @Test
    public void verifyThatWeCanInstantiateAWishListWithName() {
        WishList wishList = new WishList("My Wishes");
        assertNotNull(wishList);
        assertEquals("My Wishes", wishList.getName());
    }

    @Test
    public void verifyThatTheNewlyCreatedWishListContainsNoGifts() {
        WishList wishList = new WishList();
        wishList.getGifts();
        assertEquals(0, wishList.getGifts().size());
        assertEquals(Collections.<Gift>emptyList(), wishList.getGifts());
    }

    @Test
    public void verifyThatWeCanAddAGiftToTheWishList() {
        WishList wishList = new WishList();
        wishList.addGift(new Gift("Ball"));
        assertEquals(1, wishList.getGifts().size());
        assertEquals(new Gift("Ball"), wishList.getGifts().get(0));
    }

    @Test
    public void verifyThatWeCanAddTheSameGiftMultipleTimesToTheWishList() {
        WishList wishList = new WishList();
        wishList.addGift(new Gift("Ball"));
        wishList.addGift(new Gift("Ball"));
        assertEquals(2, wishList.getGifts().size());
        assertEquals(new Gift("Ball"), wishList.getGifts().get(0));
        assertEquals(new Gift("Ball"), wishList.getGifts().get(1));
    }

    @Test
    public void verifyThatWeCanRemoveAGiftFromTheWishList() {
        WishList wishList = new WishList();
        wishList.addGift(new Gift("Ball"));
        wishList.addGift(new Gift("Bike"));
        wishList.removeGift(new Gift("Ball"));
        assertEquals(1, wishList.getGifts().size());
        assertEquals(new Gift("Bike"), wishList.getGifts().get(0));
    }

    @Test
    public void verifyThatWeCanRemoveAllGiftsFromTheWishList() {
        WishList wishList = new WishList();
        wishList.addGift(new Gift("Ball"));
        wishList.removeGifts();
        assertEquals(0, wishList.getGifts().size());
    }

    @Test
    public void verifyThatTheToStringMethodOfAWishListIsImplemented() {
        WishList wishList = new WishList("My Wishes");
        wishList.addGift(new Gift("Ball"));
        assertEquals("WishList{name='My Wishes',gifts=1}", wishList.toString());
    }

    @Test
    public void verifyThatTheDefaultSortOrderOfAWishListIsBasedOnTheWishedDateMostRecentFirst() {
        WishList wishList = new WishList("My Wishes");
        Gift ball = new Gift("Ball");
        ball.setWishedDate(new Date());

        Gift bike = new Gift("Bike");
        bike.setWishedDate(new Date(ball.getWishedDate().getTime() - 1000));

        wishList.addGift(ball);
        wishList.addGift(bike);

        assertEquals(new Gift("Bike"), wishList.getGifts().get(0));
        assertEquals(new Gift("Ball"), wishList.getGifts().get(1));
    }


    @Test
    public void verifyThatWeCanCalculateTheTotalOfGiftsInOurWishList() {
        Gift ball = new Gift("Ball");
        Gift otherBall = new Gift("Ball");
        Gift bike = new Gift("Bike");

        WishList wishList = new WishList();
        wishList.addGift(ball);
        wishList.addGift(otherBall);
        wishList.addGift(bike);

        assertEquals(3, wishList.getTotalGiftCount());
    }

    @Test
    public void verifyThatWeCanCalculateTheTotalPriceOfTheWishesInOurWishList() {
        Gift ball = new Gift("Ball");
        ball.setPrice(new BigDecimal(10.0));

        Gift otherBall = new Gift("Ball");
        otherBall.setPrice(new BigDecimal(10.0));

        Gift bike = new Gift("Bike");
        bike.setPrice(new BigDecimal(110.0));

        WishList wishList = new WishList();
        wishList.addGift(ball);
        wishList.addGift(otherBall);
        wishList.addGift(bike);

        assertEquals(new BigDecimal(130.0), wishList.getTotalPriceOfGifts());
    }

    @Test
    public void verifyThatWeCanRetrieveTheMostExpensiveGiftFromOurWishList() {
        Gift ball = new Gift("Ball");
        ball.setPrice(new BigDecimal(10.0));

        Gift otherBall = new Gift("Ball");
        otherBall.setPrice(new BigDecimal(10.0));

        Gift bike = new Gift("Bike");
        bike.setPrice(new BigDecimal(110.0));

        WishList wishList = new WishList();
        wishList.addGift(ball);
        wishList.addGift(otherBall);
        wishList.addGift(bike);

        assertEquals(bike, wishList.getMostExpensiveGift());
    }

    @Test
    public void verifyThatWeCanRetrieveTheCheapestWishFromOurWishList() {
        Gift ball = new Gift("Ball");
        ball.setPrice(new BigDecimal(10.0));

        Gift otherBall = new Gift("Ball");
        otherBall.setPrice(new BigDecimal(10.0));

        Gift bike = new Gift("Bike");
        bike.setPrice(new BigDecimal(110.0));

        WishList wishList = new WishList();
        wishList.addGift(ball);
        wishList.addGift(otherBall);
        wishList.addGift(bike);

        assertEquals(ball, wishList.getLeastExpensiveGift());
    }

    @Test
    public void verifyThatWeCanSearchGiftsByPartOfTheGiftsName() {
        // Hint : Use contains function of String

        Gift ball = new Gift("Ball");
        Gift bike = new Gift("Bike");

        WishList wishList = new WishList();
        wishList.addGift(ball);
        wishList.addGift(bike);

        assertEquals(bike, wishList.findGiftWithNameLike("ik"));
    }

    @Test
    public void verifyThatWeCanAddTheGiftsFromAnotherWishListToOurWishList() {
        Gift ball = new Gift("Ball");
        Gift bike = new Gift("Bike");

        WishList wishList = new WishList();
        wishList.addGift(ball);
        wishList.addGift(bike);

        Gift book = new Gift("Book");
        Gift beer = new Gift("Beer");

        WishList otherWishList = new WishList();
        wishList.addGift(book);
        wishList.addGift(beer);

        wishList.addGifts(otherWishList);

        assertEquals(4, wishList.getTotalGiftCount());
    }

    @Test
    public void verifyThatWeCanSetTheMaximumNumberOfGiftsAllowedInOurWishList() {
        WishList wishList = new WishList();
        wishList.setMaximumGifts(2);
        assertEquals(2, wishList.getMaximumGifts());
    }


    @Test(expected = WishListFullException.class)
    public void verifyThatWeCantAddAGiftToAWishListThatExceedsTheMaximumNumberOfGiftsAndItThrowsAndWishListFullException() {

        // Hint : create your own WishListFullException that extends RuntimeException

        WishList wishList = new WishList();
        wishList.setMaximumGifts(2);

        Gift ball = new Gift("Ball");
        Gift book = new Gift("Book");
        Gift beer = new Gift("Beer");

        wishList.addGift(ball);
        wishList.addGift(book);
        wishList.addGift(beer);

    }

    @Test
    public void verifyThatTheWishListFullExceptionContainsANiceMessage() {

        WishList wishList = new WishList();
        wishList.setMaximumGifts(1);

        Gift ball = new Gift("Ball");
        Gift book = new Gift("Book");

        wishList.addGift(ball);
        try {
            wishList.addGift(book);
            fail(); // fail if there's no exception thrown
        } catch (WishListFullException e) {
            assertEquals("Exceeded the maximum of 1 item(s) of the wishlist", e.getMessage());
        }

    }

    @Test
    public void verifyThatAWishListFullExceptionContainsAReferenceToTheActualGift() {
        WishList wishList = new WishList();
        wishList.setMaximumGifts(1);

        Gift ball = new Gift("Ball");
        Gift book = new Gift("Book");

        wishList.addGift(ball);
        try {
            wishList.addGift(book);
            fail(); // fail if there's no exception thrown
        } catch (WishListFullException e) {
            assertEquals(book, e.getGift());
        }

    }

    @Test
    public void verifyThatWeCanDetermineTheGiftTypeBasedOnAGift() {
        Gift ball = new Gift("Ball");
        Gift bike = new Gift("Bike");
        Gift book = new Gift("Book");
        Gift pen = new Gift("Pen");
        Gift laptop = new Gift("Laptop");
        Gift paper = new Gift("Paper");
        Gift beer = new Gift("Beer");
        Gift lowerCaseBeer = new Gift("beer");


        assertEquals(GiftType.Active, WishList.determineGiftType(ball));
        assertEquals(GiftType.Active, WishList.determineGiftType(bike));
        assertEquals(GiftType.Educational, WishList.determineGiftType(book));
        assertEquals(GiftType.Educational, WishList.determineGiftType(pen));
        assertEquals(GiftType.Educational, WishList.determineGiftType(laptop));
        assertEquals(GiftType.Educational, WishList.determineGiftType(paper));
        assertEquals(GiftType.Other, WishList.determineGiftType(beer));
        assertEquals(GiftType.Other, WishList.determineGiftType(lowerCaseBeer));
    }

    @Test
    public void verifyThatWeCanDetermineTheGiftTypesBasedOnAWishList() {

        WishList wishList = new WishList();

        Gift ball = new Gift("Ball");
        Gift bike = new Gift("Bike");
        Gift pen = new Gift("Pen");
        Gift laptop = new Gift("Laptop");
        Gift paper = new Gift("Paper");

        wishList.addGift(ball);
        wishList.addGift(bike);
        wishList.addGift(pen);
        wishList.addGift(laptop);
        wishList.addGift(paper);
        wishList.addGift(ball);

        Map<GiftType, Integer> result = WishList.determineGiftType(wishList);

        assertEquals(new Integer(3), result.get(GiftType.Active));
        assertEquals(new Integer(3), result.get(GiftType.Educational));
        assertEquals(new Integer(0), result.get(GiftType.Other));
    }

     /* <- TODO Move below the test (ALT + SHIFT + <arrow down>), fix it and continue until all tests are fixed */
    }
