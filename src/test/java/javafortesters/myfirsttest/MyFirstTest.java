package javafortesters.myfirsttest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MyFirstTest {

	@Test
	public void canAddTwoPlusTwo() {
		int answer = 2 + 2;
		assertEquals("2+2=4", 4, answer);
	}

    @Test
    public void canSubtractTwoPlusTwo() {
        int answer = 2 - 2;
        assertEquals("2-2=0", 0, answer);
    }

    @Test
    public void canDivideFourByTwo() {
        int answer = 4 / 2;
        assertEquals("4/2=2", 2, answer);
    }

    @Test
    public void canMultiplyToByTo() {
        int answer = 2 * 2;
        assertEquals("2*2=4", 4, answer);
    }
}
