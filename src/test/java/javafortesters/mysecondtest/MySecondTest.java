package javafortesters.mysecondtest;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by Rob Brinkman on 9/17/2014.
 */
public class MySecondTest {

    @Test
    public void canAddZeroTwoPlusZero() {
        int answer = 0 + 0;
        assertEquals("0+0=0", 0, answer);
    }

    /*
    In one of the advanced Junit chapters we'll learn a nicer way to test for exceptions
     */
    @Test
    public void canNotDivideByZero() {
        try {
            int answer = 10 / 0;

            // Should not reach this line because exception gets thrown. If this is not the case we fail.
            fail();
        } catch (ArithmeticException arithmeticException) {
            System.out.println(arithmeticException);
            // Should throw Exception
        }

    }
}
