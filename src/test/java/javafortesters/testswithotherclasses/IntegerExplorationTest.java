package javafortesters.testswithotherclasses;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Created by rb on 17-10-14.
 */
public class IntegerExplorationTest {
	
	@Test
	public void testIntegerFromInt() {
		Integer four = new Integer(4);
		assertEquals("int value returns 4", 4, four.intValue());
	}
	
	@Test
	public void testIntegerFromString() {
		Integer five = new Integer("5");
		assertEquals("int value returns 5", 5, five.intValue());
	}
	
	@Test
	public void testAutoBoxing() {
		Integer six = 6;
		assertEquals("int value return 6", 6, six.intValue());
	}
	
	@Test
	public void testIntToHexString11BecomesB() {
		int intValue = 11;
		String hexValue = Integer.toHexString(intValue);
		assertEquals("b", hexValue);
	}
	
	@Test
	public void testIntToHexString10BecomesA() {
		int intValue = 10;
		String hexValue = Integer.toHexString(intValue);
		assertEquals("a", hexValue);
	}
	
	@Test
	public void testIntToHexString3Becomes3() {
		int intValue = 3;
		String hexValue = Integer.toHexString(intValue);
		assertEquals("3", hexValue);
	}
	
	@Test
	public void testIntToHexString21Becomes15() {
		int intValue = 21;
		String hexValue = Integer.toHexString(intValue);
		assertEquals("15", hexValue);
	}
	
	@Test
	public void testIntMinValue() {
		assertEquals(-2147483648, Integer.MIN_VALUE);
	}

	@Test
	public void testIntMaxValue() {
		assertEquals(2147483647, Integer.MAX_VALUE);
	}

    @Test
    public void testIntEqualsInt() {
        assertTrue(1==1);
    }

    @Test
    public void testIntNotEqualsInt() {
        assertTrue(1!=2);
    }

    @Test
    public void testIntLessThenInt() {
        assertTrue(1<2);
    }

    @Test
    public void testIntegerEqualsInteger() {
        assertTrue(new Integer(1).equals(new Integer(1)));
    }

    @Test
    public void testIntegerNotEqualsInteger() {
        assertTrue(!new Integer(1).equals(new Integer(2)));
    }

    @Test
    public void testIntegerLessThenInteger() {
        assertTrue(new Integer(1) < new Integer(2));
    }

    @Test
    public void testIntegerEqualsInt() {
        assertTrue(new Integer(1).equals(1));
    }

    @Test
    public void testIntegerNotEqualsInt() {
        assertTrue(!new Integer(1).equals(2));
    }

    @Test
    public void testIntegerLessThenInt() {
        assertTrue(new Integer(1) < 2);
    }
}

